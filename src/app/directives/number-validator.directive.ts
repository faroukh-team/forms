import {Directive} from '@angular/core';
import {AbstractControl, FormControl, NG_VALIDATORS, Validator, ValidatorFn} from '@angular/forms';

export function numberValidator(): ValidatorFn {

    console.log('numberValidator');

    return (control: AbstractControl): { [key: string]: any } => {

        console.log('numberValidator control.value: ', control.value);

        const isValid = !isNaN(control.value);
        return isValid ? undefined : {number: true};
    };
}

@Directive({
    selector: '[appNumberValidator]',
    providers: [{
        provide: NG_VALIDATORS,
        useExisting: NumberValidatorDirective,
        multi: true
    }],
})
export class NumberValidatorDirective implements Validator {

    validate(control: FormControl): { [key: string]: any } {
        return numberValidator()(control);
    }
}
