import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FormComponent} from './components/form/form.component';
import {TemplateFormComponent} from './components/template-form/template-form.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ReactiveFormComponent} from './components/reactive-form/reactive-form.component';
import {NumberValidatorDirective} from './directives/number-validator.directive';

const routes: Routes = [{
    path: 'template',
    component: TemplateFormComponent
}, {
    path: 'reactive',
    component: ReactiveFormComponent
}];

@NgModule({
    declarations: [
        AppComponent,
        FormComponent,
        TemplateFormComponent,
        ReactiveFormComponent,
        NumberValidatorDirective
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot(routes)
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
