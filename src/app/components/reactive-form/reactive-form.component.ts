import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {numberValidator} from '../../directives/number-validator.directive';

@Component({
    selector: 'app-reactive-form',
    templateUrl: './reactive-form.component.html',
    styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit {

    public myForm: FormGroup;

    constructor(private fb: FormBuilder) {
    }

    ngOnInit() {
        this.createForm();

        setTimeout(() => {
            this.updateForm();
        }, 3000);

        setTimeout(() => {
            this.updateCity();
        }, 5000);
    }

    public createForm() {
        // this.myForm = this.fb.group({
        //     name: ['', Validators.minLength(4)],
        //     age: [30, Validators.required],
        //     address: this.fb.group({
        //         city: '',
        //         street: ''
        //     })
        // });

        this.myForm = new FormGroup({
            name: new FormControl('', [Validators.required, Validators.minLength(4)]),
            age: new FormControl('', [Validators.required, numberValidator()]),
            address: new FormGroup({
                city: new FormControl(''),
                street: new FormControl('')
            })
        });
    }

    public updateForm() {
        this.myForm.setValue({
            name: 'John',
            age: 40,
            address: {
                city: 'New York',
                street: 'Wall Street'
            }
        });
    }

    public updateCity() {
        this.myForm.patchValue({
            address: {
                city: 'Petrove'
            }
        });
    }

    public resetForm() {
        this.myForm.reset();
    }

}
