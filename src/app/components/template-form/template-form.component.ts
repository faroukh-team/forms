import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-template-form',
    templateUrl: './template-form.component.html',
    styleUrls: ['./template-form.component.css']
})
export class TemplateFormComponent implements OnInit {

    public name: string;
    public age: string;

    constructor() {
    }

    ngOnInit() {
    }


    onSubmit() {
        console.log('onSubmit');
    }

}
